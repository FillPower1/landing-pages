$(document).ready(function () {
    var loginForm = $('.login__form')
    var registerForm = $('.register__form')

    loginForm.on('submit', auth)
    registerForm.on('submit', auth)

    function auth (e) {
        e.preventDefault()

        var targetForm = registerForm
        var authUrl = '/register'

        if ($(e.target).hasClass('login__form')) {
            targetForm = loginForm
            authUrl = '/login'
        }

        var data = {}
        targetForm.serializeArray().forEach(function(item) {
            data[item.name] = item.value
        })

        $.ajax({
            type: 'POST',
            url: authUrl,
            data: JSON.stringify(data),
            success: () => console.log(data, ' успех, данные отправлены'),
            error: () => console.log(data, 'ошибка, данные не отправлены')
        })
    }
})
