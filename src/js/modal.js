// document.addEventListener('DOMContentLoaded', () => {
//     const $login = document.getElementById('login')
//     const $register = document.getElementById('register')
//
//     const toggleModal = (event) => {
//         if (event.target.classList.contains('modal')) {
//             event.target.classList.add('modal--disabled')
//             document.body.style.overflow = ''
//
//             return
//         }
//
//         const type = event?.target?.id
//
//         if (!type) {
//             return
//         }
//
//         const auth = document.querySelector(`[data-auth=${type}]`)
//
//         auth.classList.remove('modal--disabled')
//         document.body.style.overflow = 'hidden'
//     }
//
//     $login.addEventListener('click', toggleModal)
//     $register.addEventListener('click', toggleModal)
//     document.addEventListener('click', toggleModal)
// })

$(document).ready(function () {
    var animDuration = 80
    var body = $('body')

    function toggleModal (event) {
        var type = event.target.id

        $(`[data-auth=${type}]`).fadeToggle(animDuration)
        body.css({ overflow: 'hidden'})
    }

    $('#register').on('click', toggleModal)
    $('#login').on('click', toggleModal)

    var modal = $('.modal')
    modal.on('click', function (event) {
        if ($(event.target).hasClass('modal')) {
            modal.fadeOut(80)
            body.css({ overflow: ''})
        }
    })
})
