jQuery(function($) { // DOM is now ready and jQuery's $ alias sandboxed

$(document).ready(function(){

	// arrows function
	function form_info(){
		$('.input-type').val($(".txt-type").text());
		$('.input-mark').val($(".txt-mark").text());
		$('.input-model').val($(".txt-model").text());
	};
	
	// arrows function
	function arrows(){
		if ($('.make-quick-order .main-center > div.open').index() > 0) {
			$('.make-quick-order .arrow-left').addClass('active');
			console.log(1);
		}
		else{
			$('.make-quick-order .arrow-left').removeClass('active');
			console.log(2);
		}

		if ($('.make-quick-order .main-center > div.open + .open-step').index() > $('.make-quick-order .main-center > div.open').index() ) {
			$('.make-quick-order .arrow-right').addClass('active');
			console.log(3);
		}
		else{
			$('.make-quick-order .arrow-right').removeClass('active');
		}
		form_info();
	};

	// fast ordering = basic
	$(".make-quick-order #step-1 .item").click(function(){		
		if (!$(this).hasClass('other')) {
			$('.make-quick-order .top .start-label').hide();
			$('.make-quick-order .top .item').css('display','inline-block');
		}
	});
	$(".make-quick-order .wr-all-item .item").click(function(){		
		if (!$(this).hasClass('other')) {
			$(this).parent('.wr-all-item').find('.form').slideUp(300);
		}
		
		$(this).addClass('active');
		$(this).parent('.wr-all-item').children('.item').not($(this)).removeClass('active');
	
		var step = $(this).parent('.wr-all-item').attr('id').replace('step-', '');
		var crubms_step = parseInt($(this).parent('.wr-all-item').attr('id').replace('step-', '')) - 1;
		var crumbs_txt = $(this).find('.text').text();

		if (!$(this).hasClass('other')) {
			$(this).parent('.wr-all-item').hide().removeClass('open');
			$(".make-quick-order .main-center > div:eq("+step+")").addClass('open open-step').fadeIn(300);
			$(".make-quick-order .top .wr-text > p:eq("+crubms_step+") .txt-label").text(crumbs_txt);
			$(".make-quick-order .top .wr-text > p:eq("+step+")").removeClass('txt-hidden');
		}
		arrows();
	});

	// fast ordering = other
	$(".make-quick-order .other").click(function(){
		var other = $(this).attr('class').split(' ')[2].replace('other', '');
		$(".form" + other).slideDown(300);
	});
	$(".make-quick-order .button").click(function(){
		if ($(this).siblings('.input-text').val() == "") {
			$(this).siblings('.input-text').addClass('required');
		}	
		else{
			var step = $(this).parents('.wr-all-item').attr('id').replace('step-', '');
			var crubms_step = parseInt($(this).parents('.wr-all-item').attr('id').replace('step-', '')) - 1;
			var crumbs_txt = $(this).siblings('.input-text').removeClass('required').val();
			
			$(this).parents('.wr-all-item').hide().removeClass('open');
			$(".make-quick-order .main-center > div:eq("+step+")").addClass('open open-step').fadeIn(300);	
			$(".make-quick-order .top .wr-text > p:eq("+crubms_step+") .txt-label").text(crumbs_txt);
			$(".make-quick-order .top .wr-text > p:eq("+step+")").removeClass('txt-hidden');

			$('.make-quick-order .top .start-label').hide();
			$('.make-quick-order .top .item').css('display','inline-block');
		}	
		arrows();
	});

	// fast ordering = arrows
	$(".make-quick-order .arrow-left").click(function(){	
		if ($(this).hasClass('active')) {
			var step = parseInt($(".make-quick-order .main-center > div.open").removeClass('open').hide().attr('id').replace('step-', '')) - 2;
			$(".make-quick-order .main-center > div:eq("+step+")").addClass('open').fadeIn(300);
			arrows();
		}
	});
	$(".make-quick-order .arrow-right").click(function(){	
		if ($(this).hasClass('active')) {
			var step = parseInt($(".make-quick-order .main-center > div.open").removeClass('open').hide().attr('id').replace('step-', ''));
			$(".make-quick-order .main-center > div:eq("+step+")").addClass('open').fadeIn(300);
			arrows();
		}
	});

	
});

});




