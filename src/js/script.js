jQuery(function($) { // DOM is now ready and jQuery's $ alias sandboxed

$(document).ready(function(){
	$(".questions-answers .wr-name").click(function(){
		$(this).siblings(".wr-text").slideToggle(300);
		$(".questions-answers .wr-name").not($(this)).siblings(".wr-text").slideUp(300);
	});

	$('.header a[href^="#"]').click(function(){
		var target = $(this).attr('href');
		$('html,body').animate({scrollTop: $(target).offset().top-55}, 1000);
		return false;
	});

	$(".header-account .notification").click(function(){
		$(".header-account .wr-notification").toggleClass("active");
	});

	$(".header-account .wr-lk .lk").click(function(){
		$(this).siblings(".popup-lk").slideToggle().parents(".wr-lk").toggleClass("active");
	});

	$(document).mouseup(function(e){
		if (!$(".header-account .wr-notification").is(e.target) && $(".header-account .wr-notification").has(e.target).length === 0) {
			$('.header-account .wr-notification').removeClass('active');
		}
		if (!$(".header-account .wr-lk").is(e.target) && $(".header-account .wr-lk").has(e.target).length === 0) {
			$('.header-account .wr-lk').removeClass("active");
			$('.header-account .popup-lk').slideUp();
		}
	});

	$('.all-tabs a').click(function(e) {
       	e.preventDefault();
        $('.all-tabs .active').removeClass('active');
        $(this).addClass('active');
        var tab = $(this).attr('href');
        $('.tab').not(tab).css({'display':'none'});
        $(tab).fadeIn(500);
    });

    $(".main-lk .short-info .read-full-info").click(function(){
    	$(".main-lk .all-tabs a").removeClass("active");
		$(".main-lk .all-tabs a.info").addClass("active");
		$(".main-lk .content-right #tab1").fadeOut(200);
		$(".main-lk .content-right #tab2").fadeIn(200);
	});

	$(".main-lk .item-right .show-more").click(function(){
    	$(this).hide().parents(".top").siblings(".wr-photo").slideDown();
	});

	$(".main-lk .wr-photo .roll-up").click(function(){
    	$(".main-lk .item-right .show-more").show();
		$(this).parents(".wr-photo").siblings(".top");
		$(this).parents(".wr-photo").slideUp();
	});


	var navbar = $('.main-order .content-left');
	var wrapper = $('.main-wrapper');

	$(window).scroll(function(){
		if($('.main-order .content-left').length == 1){
		    var nsc = $(document).scrollTop();
		    var bp1 = wrapper.offset().top;
		    var bp2 = bp1 + wrapper.outerHeight()-$(window).height();

		    if (nsc>bp1) {  navbar.css('position','fixed'); }
		    else { navbar.css('position','static'); }
		    if (nsc>bp2) { navbar.css('top', bp2-nsc); }
		    else { navbar.css('top', '80px'); }
		}
	});


	$(".wr-new-messages .text").each(function(){
		var size = 70,
	    newsContent= $(this),
	    newsText = newsContent.text();

		if(newsText.length > size){
			newsContent.text(newsText.slice(0, size) + ' ...');
		}
	});

	$(".wr-new-messages .new-messages").each(function(index){
		$( ".wr-new-messages .new-messages").delay(3000).fadeOut();
	});

	$(".wr-new-messages .close").click(function(){
		$(this).parents(".new-messages").hide();
	});


	$('.main-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		arrows: true,
		// autoplay: true,
		// autoplaySpeed: 4000,
	});

	$('.slider-about-company').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		infinite: true,
		arrows: true,
	});

	// facybox gallery
	if ($(".fancybox").length >= 1) {
		$(".fancybox").fancybox({
			hash : false,
			loop : true,
			keyboard : true,
			animationEffect : false,
			arrows : true,
			clickContent : false
		});
	}

	$("#phone, #phone1").mask("+7 (999) 999-99-99");

});

});




